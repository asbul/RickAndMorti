protocol AuthorizationPresentationLogic {
    func present(response: Authorization.Response.ResponseType)
}

final class AuthorizationPresenter: AuthorizationPresentationLogic {
    weak var viewController: AuthorizationDisplayLogic?
    
    func present(response: Authorization.Response.ResponseType) {
        switch response {
        case .presentUserData(userData: let userData):
            viewController?.display(viewModel: .displayUserData(
                userName: userData?.userName ?? "",
                password: userData?.password ?? ""
            ))
        case .presentStatus(isRegister: let isRegister):
            if isRegister == true {
                viewController?.display(viewModel: .passToCharacterListVC)
            } else {
                viewController?.display(viewModel: .displayNoRegisterData)
            }
        }
    }
}
