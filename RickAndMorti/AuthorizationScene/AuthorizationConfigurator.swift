import UIKit

final class AuthorizationConfigurator {
    static func configure() -> UIViewController {
        let viewController = AuthorizationViewController()
        let interactor = AuthorizationInteractor()
        let presenter = AuthorizationPresenter()
        let router = AuthorizationRouter()
        
        viewController.interactor = interactor
        viewController.router = router
        
        interactor.presenter = presenter
       
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
        
        return viewController
    }
}
