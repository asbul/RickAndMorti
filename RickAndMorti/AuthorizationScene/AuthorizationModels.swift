import Foundation

enum Authorization {
    struct Request {
        enum RequestType {
            case getRegisterData
            case checkRegisterData(userName: String, password: String)
        }
    }
    struct Response {
        enum ResponseType {
            case presentUserData(userData: RegisterData?)
            case presentStatus(isRegister: Bool)
        }
    }
    struct ViewModel {
        enum ViewModelData {
            case displayUserData(userName: String, password: String)
            case passToCharacterListVC
            case displayNoRegisterData
        }
    }
}
