import UIKit

protocol AuthorizationDisplayLogic: AnyObject {
    func display(viewModel: Authorization.ViewModel.ViewModelData)
}

final class AuthorizationViewController: UIViewController {
    var interactor: AuthorizationBusinessLogic?
    var router: (NSObjectProtocol & AuthorizationRoutingLogic & AuthorizationDataPassing)?

// MARK: - UIElements
    private let userNameTextField = UITextField.customTextField(placeholder: "User Name", fontSize: 16, returnKeyType: .next)
    private let userPasswordTextField = UITextField.customTextField(placeholder: "Password", fontSize: 16, returnKeyType: .done)

    private let logInButton = UIButton.customButton(title: "Log In", fontSize: 20)
    
    private let showStackViewButton = UIButton.customButton(title: "Show StackView Example", fontSize: 20)
    
    private let authorizationView = UIView()

    private let backgroundImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "8cdea5a04c79cf3098f19752e5bcf0d8")
        imageView.contentMode = .scaleAspectFill
        imageView.alpha = 0.6
        return imageView
    }()
    
// MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.delegate = self
        userPasswordTextField.delegate = self
        userPasswordTextField.isSecureTextEntry = true
        userNameTextField.becomeFirstResponder()

        setupBackgroundView()
        setupAuthorizationView()
        
        logInButton.addTarget(self, action: #selector(loginButtonPressed), for: .touchUpInside)
        showStackViewButton.addTarget(self, action: #selector(showStackViewButtonPressed), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        interactor?.makeRequest(request: .getRegisterData)
        
    }
    
    // MARK: - Setup UIElements
    private  func setupBackgroundView() {
        view.addSubview(backgroundImageView)
        backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

    private func setupAuthorizationView() {
        view.addSubview(authorizationView)
        authorizationView.translatesAutoresizingMaskIntoConstraints = false
        authorizationView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 40).isActive = true
        authorizationView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -40).isActive = true
        authorizationView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        authorizationView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true

        authorizationView.addSubview(userNameTextField)
        userNameTextField.leadingAnchor.constraint(equalTo: authorizationView.leadingAnchor).isActive = true
        userNameTextField.topAnchor.constraint(equalTo: authorizationView.topAnchor).isActive = true
        userNameTextField.trailingAnchor.constraint(equalTo: authorizationView.trailingAnchor).isActive = true

        authorizationView.addSubview(userPasswordTextField)
        userPasswordTextField.leadingAnchor.constraint(equalTo: authorizationView.leadingAnchor).isActive = true
        userPasswordTextField.topAnchor.constraint(equalTo: userNameTextField.bottomAnchor, constant: 10).isActive = true
        userPasswordTextField.trailingAnchor.constraint(equalTo: authorizationView.trailingAnchor).isActive = true

        view.addSubview(logInButton)
        logInButton.leadingAnchor.constraint(equalTo: authorizationView.leadingAnchor).isActive = true
        logInButton.topAnchor.constraint(equalTo: userPasswordTextField.bottomAnchor, constant: 16).isActive = true
        logInButton.trailingAnchor.constraint(equalTo: authorizationView.trailingAnchor).isActive = true
        logInButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        view.addSubview(showStackViewButton)
        NSLayoutConstraint.activate([
            showStackViewButton.topAnchor.constraint(equalTo: logInButton.bottomAnchor, constant: 16),
            showStackViewButton.leadingAnchor.constraint(equalTo: authorizationView.leadingAnchor),
            showStackViewButton.trailingAnchor.constraint(equalTo: authorizationView.trailingAnchor),
            showStackViewButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    // MARK: - @objc methods
    @objc func loginButtonPressed() {
        interactor?.makeRequest(request: .checkRegisterData(
            userName: userNameTextField.text ?? "",
            password: userPasswordTextField.text ?? ""
        ))
    }
    
    @objc private func showStackViewButtonPressed() {
        print("show stackViewExample")
        let stackVC = StackViewExampleViewController()
        stackVC.set(firstButtonTitle: userNameTextField.text ?? "",
                    secondButtonTitle: userPasswordTextField.text ?? "")
        present(stackVC, animated: true)
    }
    
    // MARK: - Private Methods
    private func showAlert() {
        let alert = UIAlertController(
            title: "No registerData",
            message: "Enter your Name and Password to see all RickAndMorty characters",
            preferredStyle: .alert
        )
        let okAction = UIAlertAction(title: "Ok", style: .default) { _ in
            self.userNameTextField.becomeFirstResponder()
        }
        alert.addAction(okAction)
        present(alert, animated: true)
    }
}

// MARK: - UIText Field Delegate
extension AuthorizationViewController: UITextFieldDelegate {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userNameTextField {
            userPasswordTextField.becomeFirstResponder()
        }
        return true
    }
}

extension AuthorizationViewController: AuthorizationDisplayLogic {
    func display(viewModel: Authorization.ViewModel.ViewModelData) {
        switch viewModel {
        case .displayUserData(userName: let userName, password: let password):
            userNameTextField.text = userName
            userPasswordTextField.text = password
        case .passToCharacterListVC:
            router?.routeToCharactersList()
        case .displayNoRegisterData:
           showAlert()
        }
    }
}
