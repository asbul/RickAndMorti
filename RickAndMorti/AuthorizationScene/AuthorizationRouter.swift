import UIKit

@objc protocol AuthorizationRoutingLogic {
    func routeToCharactersList()
}

protocol AuthorizationDataPassing {
    var dataStore: AuthorizationDataStore? { get }
}

final class AuthorizationRouter: NSObject, AuthorizationRoutingLogic, AuthorizationDataPassing {
    
    weak var viewController: AuthorizationViewController?
    var dataStore: AuthorizationDataStore?
    
    // MARK: - Routing
    
    func routeToCharactersList() {
        let charactersListVC = CharacterListConfigurator.configure()
        let navigationVC = UINavigationController(rootViewController: charactersListVC)
        navigationVC.modalPresentationStyle = .fullScreen
        viewController?.present(navigationVC, animated: true)
    }
}
