protocol AuthorizationBusinessLogic {
    func makeRequest(request: Authorization.Request.RequestType)
}

protocol AuthorizationDataStore {
    var userData: RegisterData? { get set }
    var isRegister: Bool { get set }
}

final class AuthorizationInteractor: AuthorizationDataStore {
    var userData: RegisterData?
    var isRegister: Bool = false
    
    var presenter: AuthorizationPresentationLogic?
}

extension AuthorizationInteractor: AuthorizationBusinessLogic {
    func makeRequest(request: Authorization.Request.RequestType) {
        switch request {
        case .getRegisterData:
            userData = StorageManager.shared.getRegisterData()
            presenter?.present(response: .presentUserData(userData: userData))
        case.checkRegisterData(userName: let userName, password: let password):
            isRegister = (userName != "" && password != "") ? true : false
            if isRegister == true {
                userData = RegisterData(userName: userName, password: password)
                // useData was checking before
                StorageManager.shared.saveRegister(userData: userData!)
            }
            presenter?.present(response: .presentStatus(isRegister: isRegister))
        }
    }
}
