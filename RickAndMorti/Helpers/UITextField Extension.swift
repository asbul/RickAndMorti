import UIKit

extension UITextField {
    static func customTextField(placeholder: String, fontSize: CGFloat, returnKeyType: UIReturnKeyType) -> UITextField {
        let textField = UITextField()
        textField.font = .systemFont(ofSize: fontSize)
        textField.returnKeyType = returnKeyType
        textField.textColor = .black
        textField.alpha = 0.9

        textField.attributedPlaceholder = NSAttributedString(
            string: placeholder,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray]
        )

        textField.borderStyle = .roundedRect
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }
}
