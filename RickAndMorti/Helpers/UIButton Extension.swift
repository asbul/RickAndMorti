import UIKit

extension UIButton {
    static func customButton(title: String, fontSize: CGFloat) -> UIButton {
        let button = UIButton(type: .custom)
        button.setTitle(title, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: fontSize)
        button.titleLabel?.textColor = .white
        button.backgroundColor = .black
        button.alpha = 0.8
        button.layer.cornerRadius = 5
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }
}
