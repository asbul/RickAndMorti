protocol CharacterListBusinessLogic {
    func makeRequest(request: CharacterList.Request.RequestType)
}

protocol CharacterListDataStore {
    var rickAndMorty: RickAndMorty? { get }
    var characters: [Character] { get set }
}

final class CharacterListInteractor: CharacterListDataStore {
    var rickAndMorty: RickAndMorty?
    var characters: [Character] = []
    
    var presenter: CharacterListPresentationLogic?
}

extension CharacterListInteractor: CharacterListBusinessLogic {
    func makeRequest(request: CharacterList.Request.RequestType) {
        switch request {
        case .getData:
            NetworkManager.shared.fetchData(from: Link.rickAndMortyApi.rawValue) { [weak self] rickAndMorty in
                self?.rickAndMorty = rickAndMorty
                self?.characters = rickAndMorty.results
                self?.presenter?.presentCharacters(response: .presentCharacters(characters: self?.characters ?? []))
            }
        case .getMoreData:
            NetworkManager.shared.fetchData(from: rickAndMorty?.info.next) { [weak self] rickAndMorty in
                self?.rickAndMorty = rickAndMorty
                let newCharacters = self?.rickAndMorty?.results
                self?.characters.append(contentsOf: newCharacters ?? [])
                self?.presenter?.presentCharacters(response: .presentCharacters(characters: self?.characters ?? []))
            }
        case .logOut:
            StorageManager.shared.deleteRegisterData()
        }
    }
}
