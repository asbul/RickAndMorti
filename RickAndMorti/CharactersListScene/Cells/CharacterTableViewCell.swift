import UIKit

final class CharacterTableViewCell: UITableViewCell {

    // MARK: - UI Elements
    private var characterImageView: CharacterImageView = {
        let characterImageView = CharacterImageView()
        characterImageView.bounds.size = CGSize(width: 65, height: 65)
        characterImageView.layer.cornerRadius = characterImageView.frame.width / 2
        characterImageView.backgroundColor = .black
        characterImageView.image = UIImage(named: "8cdea5a04c79cf3098f19752e5bcf0d8")
        characterImageView.contentMode = .scaleAspectFill
        characterImageView.clipsToBounds = true
        return characterImageView
    }()
    
    private var characterNameLabel = UILabel.customLabel(fontSize: 20)
    private var characterStatusLabel = UILabel.customLabel(fontSize: 14)
    private var characterGenderLabel = UILabel.customLabel(fontSize: 14)
    
    // MARK: - Object LifeCycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Setup UI Methods
    private func setupUI() {
        addSubview(characterImageView)
        characterImageView.translatesAutoresizingMaskIntoConstraints = false
        characterImageView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        characterImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5).isActive = true
        characterImageView.widthAnchor.constraint(equalToConstant: 65).isActive = true
        characterImageView.heightAnchor.constraint(equalToConstant: 65).isActive = true
        
        addSubview(characterNameLabel)
        characterNameLabel.leadingAnchor.constraint(equalTo: characterImageView.trailingAnchor, constant: 10).isActive = true
        characterNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        
        addSubview(characterStatusLabel)
        characterStatusLabel.leadingAnchor.constraint(equalTo: characterImageView.trailingAnchor, constant: 10).isActive = true
        characterStatusLabel.topAnchor.constraint(equalTo: characterNameLabel.bottomAnchor, constant: 5).isActive = true
        
        addSubview(characterGenderLabel)
        characterGenderLabel.leadingAnchor.constraint(equalTo: characterImageView.trailingAnchor, constant: 10).isActive = true
        characterGenderLabel.topAnchor.constraint(equalTo: characterStatusLabel.bottomAnchor, constant: 5).isActive = true
    }
    
    // MARK: - Character Cell Display Logic
    func configure(from character: Character) {
        characterNameLabel.text = character.name
        characterStatusLabel.text = "Status: \(character.status)"
        characterGenderLabel.text = "Gender: \(character.gender)"
        characterImageView.setImage(from: character.image)
    }    
}
