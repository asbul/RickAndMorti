import Foundation

enum CharacterList {
    struct Request {
        enum RequestType {
            case getData
            case getMoreData
            case logOut
        }
    }
    
    struct Response {
        enum ResponseType {
            case presentCharacters(characters: [Character])
        }
    }
    
    struct ViewModel {
        enum ViewModelData {
            case displayCharacters(characters: [Character])
        }
    }
}
