import UIKit

@objc protocol CharactersListRoutingLogic {
    func routeToEpisodeListForCharacter(at index: Int)
}

protocol CharactersListDataPassing {
    var dataStore: CharacterListDataStore? { get }
}

final class CharactersListRouter: NSObject, CharactersListRoutingLogic, CharactersListDataPassing {
    
    weak var viewController: CharacterListViewController?
    var dataStore: CharacterListDataStore?
    
    func routeToEpisodeListForCharacter(at index: Int) {
        let characterEpisodeListVC = CharacterEpisodeListConfigurator.configure()
        guard let sourceDS = dataStore, var destinationDS = characterEpisodeListVC.router?.dataStore else { return }
        passDataToCharacterEpisodes(source: sourceDS, destination: &destinationDS, at: index)
        viewController?.navigationController?.pushViewController(characterEpisodeListVC, animated: true)
    }
    
    func passDataToCharacterEpisodes(
        source: CharacterListDataStore,
        destination: inout CharacterEpisodeListDataStore,
        at index: Int
    ) {
        destination.episodes = source.characters[index].episode
    }
}


/*
    // MARK: Routing
    func routeToNoteDetailsForEditing(at index: Int) {
        let noteDetailsVC = NoteDetailsViewController()
        guard let dataStore = dataStore, var destinationDS = noteDetailsVC.router?.dataStore else { return }
        passDataToNoteDetails(source: dataStore, destination: &destinationDS, at: index)
        viewController?.navigationController?.pushViewController(
            noteDetailsVC,
            animated: true
        )
    }
 
    // MARK: Passing data
    func passDataToNoteDetails(
        source: NoteListDataStore,
        destination: inout NoteDetailsDataStore,
        at index: Int
    ) {
        destination.note = source.notes[index]
    }
*/
