import UIKit

protocol CharactersListDisplayLogic: AnyObject {
    func displayCharacters(viewModel: CharacterList.ViewModel.ViewModelData)
}

final class CharacterListViewController: UIViewController {
        
    var interactor: CharacterListBusinessLogic?
    var router: (NSObjectProtocol & CharactersListRoutingLogic & CharactersListDataPassing)?
    
    // MARK: - Private Properties
    private var characters: [Character] = []
    
    // MARK: - UIElements
    private var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(CharacterTableViewCell.self, forCellReuseIdentifier: "Character")
        tableView.backgroundColor = .black
        tableView.rowHeight = 80
        return tableView
    }()
    
    private var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .white
        return activityIndicator
    }()
    
    private var logOutBarButtonItem = UIBarButtonItem()
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        tableView.dataSource = self
        tableView.delegate = self
        
        setupTableView()
        setupNavigationBar()
        setupBarButtonItem()
        setupActivityIndicator()

        getData()
    }
        
    // MARK: - Setup UI Methods
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func setupNavigationBar() {
        title = "Characters"
        navigationController?.navigationBar.prefersLargeTitles = true
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.backgroundColor = .black
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        
        navigationController?.navigationBar.standardAppearance = navBarAppearance
        navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        navigationItem.backBarButtonItem?.tintColor = .white
    }
    
    private func setupBarButtonItem() {
        logOutBarButtonItem.title = "Log Out"
        navigationItem.rightBarButtonItem = logOutBarButtonItem
        logOutBarButtonItem.target = self
        logOutBarButtonItem.action = #selector(logOutAction)
    }
    
    private func setupActivityIndicator() {
        view.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    // MARK: - @objc methods
    @objc func logOutAction() {
        interactor?.makeRequest(request: .logOut)
        dismiss(animated: true)
    }

    // MARK: - Private Methods
    private func getData() {
        interactor?.makeRequest(request: .getData)
        activityIndicator.startAnimating()
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension CharacterListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        characters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Character", for: indexPath) as? CharacterTableViewCell
        else { return UITableViewCell() }
        cell.configure(from: characters[indexPath.row])
        cell.backgroundColor = .black
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        router?.routeToEpisodeListForCharacter(at: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row + 3 == characters.count {
            interactor?.makeRequest(request: .getMoreData)
        }
    }
}

extension CharacterListViewController: CharactersListDisplayLogic {
    func displayCharacters(viewModel: CharacterList.ViewModel.ViewModelData) {
        switch viewModel {
        case .displayCharacters(characters: let characters):
            self.characters = characters
            tableView.reloadData()
            activityIndicator.stopAnimating()
        }
    }
    
}
