protocol CharacterListPresentationLogic {
    func presentCharacters(response: CharacterList.Response.ResponseType)
}

final class CharacterListPresenter: CharacterListPresentationLogic {
    weak var viewController: CharactersListDisplayLogic?
    
    func presentCharacters(response: CharacterList.Response.ResponseType) {
        switch response {
        case .presentCharacters(characters: let characters):
            viewController?.displayCharacters(viewModel: .displayCharacters(characters: characters))
        }
    }
}
