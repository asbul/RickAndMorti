//
//  StackViewExampleViewController.swift
//  RickAndMorti
//
//  Created by asbul on 30.08.2023.
//

import UIKit

fileprivate enum ViewMetrics {
    static let standartOffset: CGFloat = 20
    static let fontSize: CGFloat = 20
}

final class StackViewExampleViewController: UIViewController {
    
    private let firstButton = UIButton.customButton(title: "FirstButtonTitle", fontSize: 20)
    private let secondButton = UIButton.customButton(title: "Second", fontSize: 20)
    
    private lazy var buttonStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [firstButton, secondButton])
        stackView.distribution = .fillEqually
        stackView.spacing = ViewMetrics.standartOffset
        stackView.backgroundColor = .yellow
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        constraintViews()
        configureAppearance()
    }
    
    private func setupViews() {
        view.addSubview(buttonStackView)
    }
    
    private func constraintViews() {
        NSLayoutConstraint.activate([
            buttonStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            buttonStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: ViewMetrics.standartOffset),
            buttonStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -ViewMetrics.standartOffset),
        ])
    }
    
    private func configureAppearance() {
        view.backgroundColor = .white
    }
    
    func set(firstButtonTitle: String, secondButtonTitle: String) {
        firstButton.setTitle(firstButtonTitle, for: .normal)
        secondButton.setTitle(secondButtonTitle, for: .normal)
        let normalButtonWidth = (view.frame.width - ViewMetrics.standartOffset * 3) / 2
        if firstButton.intrinsicContentSize.width > normalButtonWidth || secondButton.intrinsicContentSize.width > normalButtonWidth {
            buttonStackView.distribution = .fillProportionally
        }
    }
}
