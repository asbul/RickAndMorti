import Foundation
import Kingfisher

class CharacterImageView: UIImageView {
    func setImage(from imageUrl: String?) {
        guard let imageUrl = URL(string: imageUrl ?? "") else { return }
        self.kf.setImage(with: imageUrl)
    }
}
