import Foundation

struct RegisterData: Codable {
    var userName: String
    var password: String
}
