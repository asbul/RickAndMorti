import Foundation

struct RickAndMorty: Decodable {
    let info: Info
    let results: [Character]
}

struct Info: Decodable {
    let pages: Int
    let next: String?
    let prev: String?
}

struct Character: Decodable {
    let name: String
    let status: String
    let gender: String
    let image: String
    let episode: [String]
}

enum Link: String {
    case rickAndMortyApi = "https://rickandmortyapi.com/api/character"
}
