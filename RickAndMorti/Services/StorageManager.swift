import Foundation

final class StorageManager {
    static let shared = StorageManager()
    
    private let ud = UserDefaults.standard
    private let key = "registerDataKey"
    
    private init() {}
    
    func saveRegister(userData: RegisterData) {
        guard let data = try? JSONEncoder().encode(userData) else { return }
        ud.set(data, forKey: key)
    }
    
    func getRegisterData() -> RegisterData? {
        guard let data = ud.data(forKey: key) else { return nil }
        guard let userData = try? JSONDecoder().decode(RegisterData.self, from: data) else { return nil }
        return userData
    }
    
    func deleteRegisterData() {
        ud.removeObject(forKey: key)
    }
}
