protocol CharacterEpisodeListBusinessLogic {
    func makeRequest(request: CharacterEpisodeList.Request.RequestType)
}

protocol CharacterEpisodeListDataStore {
    var episodes: [String] { get set }
}

final class CharacterEpisodeListInteractor: CharacterEpisodeListDataStore {
    var presenter: CharacterEpisodeListPresentationLogic?
    var episodes: [String] = []
    
}

extension CharacterEpisodeListInteractor: CharacterEpisodeListBusinessLogic {
    func makeRequest(request: CharacterEpisodeList.Request.RequestType) {
        switch request {
        case .provideEpisodes:
            presenter?.presentEpisodes(response: .presentEpisodes(episodes: episodes))
        }
    }
}
