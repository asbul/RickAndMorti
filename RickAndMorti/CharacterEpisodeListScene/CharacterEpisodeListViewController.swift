import UIKit

protocol CharacterEpisodeListDisplayLogic: AnyObject {
    func displayEpisodes(viewModel: CharacterEpisodeList.ViewModel.ViewModelData)
}

final class CharacterEpisodeListViewController: UITableViewController {
        
    var interactor: CharacterEpisodeListBusinessLogic?
    var router: (NSObjectProtocol & CharacterEpisodeListRoutingLogic & CharacterEpisodeListDataPassing)?
    
    // MARK: - Private Properties
    var episodes: [String] = []
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Episodes"
        view.backgroundColor = .black
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Episode")
        interactor?.makeRequest(request: .provideEpisodes)
    }

    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        episodes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Episode", for: indexPath)
        cell.backgroundColor = .black
        let episode = episodes[indexPath.row]
        
        var content = cell.defaultContentConfiguration()
        content.textProperties.color = .white
        content.textProperties.font = UIFont.systemFont(ofSize: 16)
        content.text = "Episode: #\(episode)"
        cell.contentConfiguration = content
        
        return cell
    }
}

extension CharacterEpisodeListViewController: CharacterEpisodeListDisplayLogic {
    func displayEpisodes(viewModel: CharacterEpisodeList.ViewModel.ViewModelData) {
        switch viewModel {
        case .displayEpisodes(episodes: let episodes):
            self.episodes = episodes
        }
    }
}
