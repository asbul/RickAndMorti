import Foundation

enum CharacterEpisodeList {
    struct Request {
        enum RequestType {
            case provideEpisodes
        }
    }
    
    struct Response {
        enum ResponseType {
            case presentEpisodes(episodes: [String])
        }
    }
    
    struct ViewModel {
        enum ViewModelData {
            case displayEpisodes(episodes: [String])
        }
    }
}
