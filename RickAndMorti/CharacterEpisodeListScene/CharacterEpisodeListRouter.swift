import UIKit

@objc protocol CharacterEpisodeListRoutingLogic {
    //func routeToSomewhere(segue: UIStoryboardSegue?)
}

protocol CharacterEpisodeListDataPassing {
    var dataStore: CharacterEpisodeListDataStore? { get }
}

final class CharacterEpisodeListRouter: NSObject, CharacterEpisodeListRoutingLogic, CharacterEpisodeListDataPassing {
    
    weak var viewController: CharacterEpisodeListViewController?
    var dataStore: CharacterEpisodeListDataStore?
}
