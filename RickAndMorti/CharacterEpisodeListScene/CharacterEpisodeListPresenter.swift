import Foundation

protocol CharacterEpisodeListPresentationLogic {
    func presentEpisodes(response: CharacterEpisodeList.Response.ResponseType)
}

class CharacterEpisodeListPresenter: CharacterEpisodeListPresentationLogic {
    weak var viewController: CharacterEpisodeListDisplayLogic?

    func presentEpisodes(response: CharacterEpisodeList.Response.ResponseType) {
        switch response {
        case .presentEpisodes(episodes: let episodes):
            let episodeNumbers = getEpisodeNumbers(episodes: episodes)
            viewController?.displayEpisodes(viewModel: .displayEpisodes(episodes: episodeNumbers))
        }
    }
    
    private func getEpisodeNumber(from episode: String) -> String {
        var episodeNumber = episode
        let range = episode.startIndex...episode.index(episode.startIndex, offsetBy: 39)
        episodeNumber.removeSubrange(range)
        return episodeNumber
    }
    
    private func getEpisodeNumbers(episodes: [String]) -> [String] {
        episodes.compactMap { getEpisodeNumber(from: $0)}
    }
}
