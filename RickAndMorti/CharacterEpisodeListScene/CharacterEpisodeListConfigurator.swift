import UIKit

final class CharacterEpisodeListConfigurator {
    static func configure() -> CharacterEpisodeListViewController {
        let tableVC = CharacterEpisodeListViewController()
        let interactor = CharacterEpisodeListInteractor()
        let presenter = CharacterEpisodeListPresenter()
        let router = CharacterEpisodeListRouter()

        tableVC.interactor = interactor
        tableVC.router = router
        interactor.presenter = presenter
        presenter.viewController = tableVC
        
        router.viewController = tableVC
        router.dataStore = interactor
        return tableVC
    }
}
